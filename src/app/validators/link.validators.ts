import {FormControl} from '@angular/forms';

export class LinkValidators {

  static checkLink(control: FormControl): {[key: string]: boolean} {
    if (control.value) {
      if (!(control.value.indexOf('imdb.com') > -1)) {
        return {wrongLink: true};
      }
    }
    return null;
  }
}
