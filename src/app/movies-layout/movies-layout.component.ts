import {Component, ComponentFactoryResolver, OnInit, ViewChild} from '@angular/core';
import {RefDirective} from '../directives/ref.directive';
import {MovieAddModalComponent} from './movie-add-modal/movie-add-modal.component';

export interface Movie {
  title: string;
  link: string;
  id?: number;
  bg?: string;
}

@Component({
  selector: 'app-movies-layout',
  templateUrl: './movies-layout.component.html',
  styleUrls: ['./movies-layout.component.scss']
})
export class MoviesLayoutComponent implements OnInit {
  @ViewChild(RefDirective, {static: false}) refDir: RefDirective;
  movies: Movie[] = [
    {id: 1, title: 'Breaking Bad', link: 'https://www.imdb.com/title/tt0903747/?ref_=nv_sr_1?ref_=nv_sr_1', bg: ''},
    {id: 2, title: 'Ex Machina', link: 'https://www.imdb.com/title/tt0470752/?ref_=nv_sr_1?ref_=nv_sr_1'},
    {id: 3, title: 'Interstellar', link: 'https://www.imdb.com/title/tt0816692/?ref_=nv_sr_1?ref_=nv_sr_1'},
    {id: 4, title: 'Fauda', link: 'https://www.imdb.com/title/tt4565380/?ref_=nv_sr_1?ref_=nv_sr_1'},
    {id: 5, title: 'Spirited Away', link: 'https://www.imdb.com/title/tt0245429/?ref_=nv_sr_1?ref_=nv_sr_1'},

  ];
  master = '#404040';
  masterColor: any;

  constructor(
    private resolver: ComponentFactoryResolver
  ) { }

  ngOnInit() {
  }

  removeMovie(id: number) {
    this.movies = this.movies.filter(p => p.id !== id);
  }

  updateMovies(movie: Movie) {
    this.movies.unshift(movie);

  }

  showModal() {
    const modalFactory = this.resolver.resolveComponentFactory(MovieAddModalComponent);
    this.refDir.containerRef.clear();
    const component = this.refDir.containerRef.createComponent(modalFactory);

    component.instance.title = 'Movie Title';
    component.instance.master = this.master;
    component.instance.lastItemId = this.movies.length;
    component.instance.close.subscribe(() => {
      this.refDir.containerRef.clear();
    });
    component.instance.onAdd.subscribe((value) => {
      this.updateMovies(value);
    });
  }

  changeBg(value) {
    this.movies.find(element => element.id === value.id).bg = value.bg;
  }

  dropColors() {
    if (this.masterColor.trim()) {
      this.master = this.masterColor;
    }
  }
}
