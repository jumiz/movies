import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Movie} from '../movies-layout.component';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent implements OnInit {
  @Input() movie: Movie;
  @Input() bg: string;
  @Input() master: string;
  @Output() onRemove = new EventEmitter<number>();
  @Output() onChangeBg = new EventEmitter<{bg: string, id: number}>();

  constructor() { }

  ngOnInit() {
  }

  removeMovie() {
    this.onRemove.emit(this.movie.id);
  }

  changeBg() {
    this.onChangeBg.emit({bg: this.bg, id: this.movie.id});
  }
}
