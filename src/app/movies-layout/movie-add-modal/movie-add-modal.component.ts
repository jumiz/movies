import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Movie} from '../movies-layout.component';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LinkValidators} from '../../validators/link.validators';

@Component({
  selector: 'app-movie-add-modal',
  templateUrl: './movie-add-modal.component.html',
  styleUrls: ['./movie-add-modal.component.scss']
})
export class MovieAddModalComponent implements OnInit {
  @Input() title: string;
  @Input() master;
  @Input() lastItemId;
  @Output() close = new EventEmitter<void>();
  @Output() onAdd: EventEmitter<Movie> = new EventEmitter<Movie>();
  form: FormGroup;

  constructor() { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.maxLength(60)]),
      link: new FormControl('', [
        Validators.required,
        Validators.maxLength(80),
        LinkValidators.checkLink
      ])
    });
  }

  submit() {
    const movie: Movie = {
      id: ++this.lastItemId,
      title: this.form.get('title').value,
      link: this.form.get('link').value
    };
    console.log('movie', movie);
    this.onAdd.emit(movie);
    this.close.emit();
    this.form.reset();
  }
}
