import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MoviesLayoutComponent} from './movies-layout/movies-layout.component';


const routes: Routes = [
  {path: 'movies', component: MoviesLayoutComponent},
  {path: '', redirectTo: 'movies', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
