import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MoviesLayoutComponent } from './movies-layout/movies-layout.component';
import { MovieCardComponent } from './movies-layout/movie-card/movie-card.component';
import { MovieAddModalComponent } from './movies-layout/movie-add-modal/movie-add-modal.component';
import {RefDirective} from './directives/ref.directive';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    MoviesLayoutComponent,
    MovieCardComponent,
    MovieAddModalComponent,
    RefDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [MovieAddModalComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
